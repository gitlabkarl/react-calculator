import React, { useState } from 'react';

import { Button } from '../Button';
import './Calculator.css';

const Calculator = () => {

    const [firstOperand, setFirstOperand] = useState(null);
    const [hasSecondOperand, setHasSecondOperand] = useState(false);
    const [value, setValue] = useState(0);
    const [operator, setOperator] = useState(null);
    const [cacheOperator, setCacheOperator] = useState(null);

    const handleBtnClick = content => {

        let curValue = value;
        let newValue = (curValue + content).toString();

        /**
         * prevent duplicate of "."
         * "." at first will have 0.number
         * prevent duplicate of "%"
         */
        if (content === "." && curValue.toString().indexOf(".") >= 1) {
            if (cacheOperator !== null && firstOperand !== null) {
                content = "0.";
            }
            else return false;
        }
        else if (content === "%" && curValue.toString().indexOf("%") >= 1) {
            return false;
        }

        // clear all display and other state
        if (content === "c") {
            setValue(0);
            setFirstOperand(null);
            setOperator(null);
            setCacheOperator(null);
            return;
        }
        else if (content === "rem") { // remove the last entered content
            let remContent = value.toString().slice(0, -1);
            remContent = (remContent === "") ? 0 : remContent;
            setValue(remContent);

            // only apply this if the first operand is not null
            // in this way the last calculated result and modified numbers will be the new first operand
            if (firstOperand !== null && operator === null) {
                setFirstOperand(null);
            }
            else if (hasSecondOperand === false) {
                setOperator(null);
                setCacheOperator(null);
            }

            return;
        }
        else if (
            content === "+" ||
            content === "-" ||
            content === "*" ||
            content === "/"
        ) {

            // to prevent the calculation when pressing the operator multiple times
            if (cacheOperator !== null) {
                setCacheOperator(content);
                setOperator(content);
                return;
            }

            // when the above condition is false, meaning the operator just press once or press at first time
            // set the digit added to first operand
            // I set it like this because the moment the user press the operator meaning they inputted the final digit to be calculated
            if (firstOperand === null || operator === null) {
                setFirstOperand(value);
            }
            else {

                // if the first operand is not null, meaning user already added one
                // the next time the operator press, it will calculate the first operand with the operator and the value will act as the second operand
                // perform the calcuation and the result will now be the first operand.
                let newFirstOperand = setEqual();
                if (newFirstOperand !== null)
                    setFirstOperand(newFirstOperand);

                setHasSecondOperand(false);
            }

            // set the new pressed operator to operators state
            // cache operator also will used to check what is the active operator, this will have the background to see what was selected operator
            setCacheOperator(content);
            setOperator(content);
            return;
        }
        else if (content === '%') {

            // if percent, set the cache operator so it will have a class "active", so user would know that they pressed the percent
            // set the value as first operand to it will be calculated
            setCacheOperator(content);
            setFirstOperand(value);

            // perform the calculation and set the result as the new first operand.
            let newFirstOperand = setEqual(content);
            if (newFirstOperand !== null)
                setFirstOperand(newFirstOperand);

            return;
        }
        else if (content === "=") {

            // it should have the first operand in order to perform the calculation
            if (firstOperand !== null) {

                // the result will be the new first operand.
                let newFirstOperand = setEqual();
                if (newFirstOperand !== null)
                    setFirstOperand(newFirstOperand);

                setHasSecondOperand(false);
            }

            // set cache operators to null. so it will be new again
            setCacheOperator(null);
            setOperator(null);
            return;
        }
        else if (content === ".") {
            // do nothing
            // I just added this to make the else not fired when pressing the "."
        }
        else {
            // dont convert to parseFloat in order to not covert the 0.0 to 0
            // if its just 0, then no duplication of 0 when multiply pressing it.
            if (newValue.toString().indexOf(".") < 1)
                newValue = parseFloat(newValue);

            if (cacheOperator !== null) {
                // reset the display to the first inputted number after adding a operator
                newValue = content;

                // set opeator to null
                // so this condition will not perform again
                // instead it will just continue adding number to display.
                setCacheOperator(null);

                setHasSecondOperand(true);
            }
        }

        // what ever go through here will be in the display.
        setValue(newValue.toString());
    }

    // this will do the arithmetic.
    // based on the first operand, operator and the last content or value that will be treated as second operand.
    const setEqual = (_operator = null) => {

        let total = null;
        let firstNum = parseFloat(firstOperand);
        let secondNum = parseFloat(value);


        let operation = (_operator !== null) ? _operator : operator;

        if (operation !== null) {
            if (operation === "+") total = parseFloat(firstNum + secondNum);
            if (operation === "-") total = parseFloat(firstNum - secondNum);
            if (operation === "*") total = parseFloat(firstNum * secondNum);
            if (operation === "/") total = parseFloat(firstNum / secondNum);
            if (operation === "%") total = parseFloat(secondNum / 100);


            if (operation !== "%") {
                let decimal = operandMax([firstNum, secondNum]);
                total = total.toFixed(decimal);
            }

            setValue(total);
        }

        return total;
    }

    const operandMax = (operand) => {

        // this will get the max length of character after the decimal point
        // and this will be used in toFixed.
        // example: 13.123 + 12.11 = 25.232999999999997
        let max = 0;
        operand.map((val) => {
            let count = (val.toString().indexOf(".") > 0) ? val.toString().length - val.toString().indexOf(".") - 1 : 0;
            return max = (count > max) ? count : max;
        });
        return max;
    }

    return (
        <div>
            <div className="calc__container">
                <h1>React</h1>
                <div className="calc__frame">
                    <div className="calc__display">
                        {value}
                    </div>
                    <div className="calc__buttons">
                        <Button onBtnClick={handleBtnClick} label="c" value="c" cclass="operator" />
                        <Button onBtnClick={handleBtnClick} label="÷" value="/" cclass="operator" activeOperator={cacheOperator} />
                        <Button onBtnClick={handleBtnClick} label="×" value="*" cclass="operator" activeOperator={cacheOperator} />
                        <Button onBtnClick={handleBtnClick} label="⌫" value="rem" cclass="backspace" />

                        <Button onBtnClick={handleBtnClick} label="7" value="7" />
                        <Button onBtnClick={handleBtnClick} label="8" value="8" />
                        <Button onBtnClick={handleBtnClick} label="9" value="9" />
                        <Button onBtnClick={handleBtnClick} label="−" value="-" cclass="operator" activeOperator={cacheOperator} />

                        <Button onBtnClick={handleBtnClick} label="4" value="4" />
                        <Button onBtnClick={handleBtnClick} label="5" value="5" />
                        <Button onBtnClick={handleBtnClick} label="6" value="6" />
                        <Button onBtnClick={handleBtnClick} label="+" value="+" cclass="operator" activeOperator={cacheOperator} />

                        <Button onBtnClick={handleBtnClick} label="1" value="1" />
                        <Button onBtnClick={handleBtnClick} label="2" value="2" />
                        <Button onBtnClick={handleBtnClick} label="3" value="3" />
                        <Button onBtnClick={handleBtnClick} label="=" value="=" cclass="equal" />

                        <Button onBtnClick={handleBtnClick} label="%" value="%" activeOperator={cacheOperator} />
                        <Button onBtnClick={handleBtnClick} label="0" value="0" />
                        <Button onBtnClick={handleBtnClick} label="." value="." />
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Calculator }
