import React from 'react';

import './Button.css';

const Button = (props) => {

    let { label, value, cclass, onBtnClick, activeOperator } = props;

    return (
        <React.Fragment>
            <button onClick={() => onBtnClick(value)} className={`calc__button ${(cclass !== undefined) ? cclass : "'"} ${(activeOperator === value) ? "active" : ""}`} >{label}</button>
        </React.Fragment>
    )
}

export { Button }
